const firstName = prompt("Enter your name: ");
const lastName = prompt("Enter your last name: ");
const birthday = new Date(prompt("Enter your date of birthday: dd.mm.yyyy"));

function CreateNewUser(firstName, lastName, birthday) {
  return  {
    firstName,
    lastName,
    birthday,
    getAge: function () {
      const ageInTimestamp = Date.now() - this.birthday;
      return Math.floor(ageInTimestamp / (1000 * 60 * 60 * 24 * 30 * 12));
    },
    getPassword: function () {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
    },
  };
}

console.log(new CreateNewUser(lastName, firstName, birthday).getAge());
console.log(new CreateNewUser(lastName, firstName, birthday).getPassword());
